#pragma once
// Math constants
#define _USE_MATH_DEFINES
#include <cmath>  
#include <random>

// Std. Includes
#include <string>
#include <time.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_operation.hpp>
#include "glm/ext.hpp"

// Other Libs
#include "SOIL2/SOIL2.h"

// project includes
#include "Application.h"
#include "Shader.h"
#include "Mesh.h"
#include "Particle.h"


// time
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

// main function
int main()
{
	// create application
	Application app = Application::Application();
	app.initRender();
	Application::camera.setCameraPosition(glm::vec3(0.0f, 5.0f, 20.0f));
			
	// create ground plane
	Mesh plane = Mesh::Mesh(Mesh::QUAD);
	// scale it up x5
	plane.scale(glm::vec3(5.0f, 5.0f, 5.0f));
	plane.setShader(Shader("resources/shaders/physics.vert", "resources/shaders/physics.frag"));

	// Create the cube objects
	Mesh cube = Mesh::Mesh("resources/models/cube.obj");
	cube.translate(glm::vec3(0.0f, 5.0f, 0.0f));
	cube.scale(glm::vec3(10.0f, 10.0f, 10.0f));
	cube.setShader(Shader("resources/shaders/physics.vert", "resources/shaders/solid_transparent.frag"));

	// time
	GLfloat firstFrame = (GLfloat) glfwGetTime();

	/*TASK 1 - VARIABLES*/
	Particle boxParticle = Particle();
	boxParticle.getMesh().setShader(Shader("resources/shaders/solid.vert", "resources/shaders/solid_blue.frag"));
	boxParticle.translate(glm::vec3(0.0f, 5.0f, 0.0f));
	boxParticle.setAcc(glm::vec3(0, -9.8f, 0));
	boxParticle.setVel(glm::vec3(5.0f, 6.0f, 8.0f));
	/********************************************************/

	/*TASK 2 - VARIABLES*/
	// NB - The particle's velocity and position are set once the correct key is pressed.
	const double dt = 0.01;
	double startTime = (GLfloat) glfwGetTime();
	double accumulator = 0.0;
	Particle boxParticle2 = Particle();
	boxParticle2.getMesh().setShader(Shader("resources/shaders/solid.vert", "resources/shaders/solid_blue.frag"));
	boxParticle2.setAcc(glm::vec3(0, -9.8f, 0));

	/*TASK 3 - VARIABLES*/
	// NB - The particles' velocities and positions are set once the correct key is pressed.
	Particle sieBoxParticle = Particle();
	sieBoxParticle.getMesh().setShader(Shader("resources/shaders/solid.vert", "resources/shaders/solid_blue.frag"));
	sieBoxParticle.setAcc(glm::vec3(0, -9.8f, 0));
	Particle forwardBoxParticle = Particle();
	forwardBoxParticle.getMesh().setShader(Shader("resources/shaders/solid.vert", "resources/shaders/solid_red.frag"));
	forwardBoxParticle.setAcc(glm::vec3(0, -9.8f, 0));
	Particle immobileBoxParticle = Particle();
	immobileBoxParticle.getMesh().setShader(Shader("resources/shaders/solid.vert", "resources/shaders/solid_green.frag"));
	immobileBoxParticle.setAcc(glm::vec3(0, 0.0f, 0));

	/*TASK 4 - VARIABLES*/
	// The cone used to visualise the blow dryer.
	Mesh cone = Mesh::Mesh("resources/models/cone_frustum.obj");
	cone.translate(glm::vec3(-0.5f, -1.0f, 0.0f));
	cone.scale(glm::vec3(2.0f, 2.0f, 2.0f));
	cone.rotate((GLfloat)M_PI*106.5/180, glm::vec3(-1.0f, 0.0f, 0.0f));
	cone.setShader(Shader("resources/shaders/physics.vert", "resources/shaders/solid_black_transparent.frag"));
	float coneRadius = 2.5f;
	float coneHeight = 3.0f;
	float coneAngleTang = coneRadius / coneHeight;

	// The particle bouncing around.
	// NB - The particle velocity, accelatation and position are set once the correct key is pressed.
	Particle particleBlowDryer = Particle();
	particleBlowDryer.getMesh().setShader(Shader("resources/shaders/solid.vert", "resources/shaders/solid_blue.frag"));
	particleBlowDryer.setMass(0.8f);
	float airDensity = 1.225f;
	glm::vec3 g = glm::vec3(0.0f, -9.8f, 0.0f);
	glm::vec3 Fg = g * particleBlowDryer.getMass();
	glm::vec3 windVel = glm::vec3(0.0f, 50.0f, 0.0f);
	
	// Booleans used to swithc between the tasks.
	bool task1 = true;
	bool task2 = false;
	bool task3 = false;
	bool task4 = false;

	// Game loop
	while (!glfwWindowShouldClose(app.getWindow()))
	{
		if (app.keys[GLFW_KEY_1]) {
			task1 = true;
			task2 = false;
			task3 = false;
			task4 = false;

			firstFrame = (GLfloat)glfwGetTime();
			boxParticle.setPos(glm::vec3(0.0f, 5.0f, 0.0f));
			boxParticle.setVel(glm::vec3(5.0f, 6.0f, 8.0f));
		}
		else if (app.keys[GLFW_KEY_2]) {
			task2 = true;
			task1 = false;
			task3 = false;
			task4 = false;

			startTime = (GLfloat)glfwGetTime();
			accumulator = 0.0;
			boxParticle2.setPos(glm::vec3(0.0f, 5.0f, 0.0f));
			boxParticle2.setVel(glm::vec3(5.0f, 6.0f, 8.0f));
		}
		else if (app.keys[GLFW_KEY_3]) {
			task3 = true;
			task1 = false;
			task2 = false;
			task4 = false;

			startTime = (GLfloat)glfwGetTime();
			accumulator = 0.0;
			sieBoxParticle.setVel(glm::vec3(0.0f, 0.0f, 0.0f));
			sieBoxParticle.setPos(glm::vec3(0.0f, 2.0f, 0.0f));
			forwardBoxParticle.setVel(glm::vec3(0.0f, 0.0f, 0.0f));
			forwardBoxParticle.setPos(glm::vec3(0.2f, 2.0f, 0.0f));
			immobileBoxParticle.setVel(glm::vec3(0.0f, 0.0f, 0.0f));
			immobileBoxParticle.setPos(glm::vec3(0.4f, 2.0f, 0.0f));
		}
		else if (app.keys[GLFW_KEY_4]) {
			task4 = true;
			task1 = false;
			task2 = false;
			task3 = false;

			startTime = (GLfloat)glfwGetTime();
			accumulator = 0.0;
			particleBlowDryer.setPos(glm::vec3(0.5f, 5.0f, 0.0f));
			particleBlowDryer.setAcc(glm::vec3(0, -9.8f, 0));
			particleBlowDryer.setVel(glm::vec3(2.0f, 5.0f, 0.0f));
		}

		/*
		**	INTERACTION
		*/
		// Manage interaction
		app.doMovement(dt);

		/*
		**	SIMULATION
		*/

		/*TASK 1*/
		if (task1) {
			/*Task 1 - times*/
			GLfloat currentFrame = (GLfloat)glfwGetTime() - firstFrame;
			currentFrame *= 1.5f;
			deltaTime = currentFrame - lastFrame;
			lastFrame = currentFrame;

			boxParticle.setVel(boxParticle.getVel() + deltaTime * boxParticle.getAcc());
			boxParticle.translate(deltaTime * boxParticle.getVel());

			// Check for collisions
			for (int index = 0; index <= 2; index++) {
				if (boxParticle.getPos()[index] < (cube.getPos()[index] - 0.5f * cube.getScale()[index][index])) {
					boxParticle.setVel(index, boxParticle.getVel()[index] * -1.0f);
					boxParticle.setVel(boxParticle.getVel() * 0.95f);
					boxParticle.setPos(index, (cube.getPos()[index] - 0.5f * cube.getScale()[index][index]));
				}
				else if (boxParticle.getPos()[index] > (cube.getPos()[index] + 0.5f * cube.getScale()[index][index])) {
					boxParticle.setVel(index, boxParticle.getVel()[index] * -1.0f);
					boxParticle.setVel(boxParticle.getVel() * 0.95f);
					boxParticle.setPos(index, (cube.getPos()[index] + 0.5f * cube.getScale()[index][index]));
				}
			}
		}
		/********************************************************/

		/*TASK 2*/
		else if (task2) {
			/*Task 2 - times*/
			GLfloat newTime = (GLfloat)glfwGetTime();
			GLfloat frameTime = newTime - startTime;
			startTime = newTime;
			accumulator += frameTime;

			while (accumulator >= dt) {
				boxParticle2.setVel(boxParticle2.getVel() + dt * boxParticle2.getAcc());
				boxParticle2.translate(dt * boxParticle2.getVel());

				// Check for collisions
				for (int index = 0; index <= 2; index++) {
					if (boxParticle2.getPos()[index] < (cube.getPos()[index] - 0.5f * cube.getScale()[index][index])) {
						boxParticle2.setVel(index, boxParticle2.getVel()[index] * -1.0f);
						boxParticle2.setVel(boxParticle2.getVel() * 0.95f);
						boxParticle2.setPos(index, (cube.getPos()[index] - 0.5f * cube.getScale()[index][index]));
					}
					else if (boxParticle2.getPos()[index] > (cube.getPos()[index] + 0.5f * cube.getScale()[index][index])) {
						boxParticle2.setVel(index, boxParticle2.getVel()[index] * -1.0f);
						boxParticle2.setVel(boxParticle2.getVel() * 0.95f);
						boxParticle2.setPos(index, (cube.getPos()[index] + 0.5f * cube.getScale()[index][index]));
					}
				}
				accumulator -= dt;
			}
		}
		/********************************************************/
		
		/*TASK 3*/
		//Semi-implicit Euler
		else if (task3) {
			GLfloat newTime = (GLfloat)glfwGetTime();
			GLfloat frameTime = newTime - startTime;
			startTime = newTime;
			accumulator += frameTime;

			while (accumulator >= dt) {
				// Semi-Implicit Euler integration - Blue quad
				sieBoxParticle.setVel(sieBoxParticle.getVel() + dt * sieBoxParticle.getAcc());
				sieBoxParticle.translate(dt * sieBoxParticle.getVel());

				// Forward Euler integration - Red quad
				forwardBoxParticle.translate(dt * forwardBoxParticle.getVel());
				forwardBoxParticle.setVel(forwardBoxParticle.getVel() + dt * forwardBoxParticle.getAcc());
				
				// Check for collisions
				for (int index = 0; index <= 2; index++) {
					if (sieBoxParticle.getPos()[index] < (cube.getPos()[index] - 0.5f * cube.getScale()[index][index])) {
						sieBoxParticle.setVel(index, sieBoxParticle.getVel()[index] * -1.0f);
						sieBoxParticle.setPos(index, (cube.getPos()[index] - 0.5f * cube.getScale()[index][index]));
					}
					else if (sieBoxParticle.getPos()[index] > (cube.getPos()[index] + 0.5f * cube.getScale()[index][index])) {
						sieBoxParticle.setVel(index, sieBoxParticle.getVel()[index] * -1.0f);
						sieBoxParticle.setPos(index, (cube.getPos()[index] + 0.5f * cube.getScale()[index][index]));
					}

					if (forwardBoxParticle.getPos()[index] < (cube.getPos()[index] - 0.5f * cube.getScale()[index][index])) {
						forwardBoxParticle.setVel(index, forwardBoxParticle.getVel()[index] * -1.0f);
						forwardBoxParticle.setPos(index, (cube.getPos()[index] - 0.5f * cube.getScale()[index][index]));
					}
					else if (forwardBoxParticle.getPos()[index] > (cube.getPos()[index] + 0.5f * cube.getScale()[index][index])) {
						forwardBoxParticle.setVel(index, forwardBoxParticle.getVel()[index] * -1.0f);
						forwardBoxParticle.setPos(index, (cube.getPos()[index] + 0.5f * cube.getScale()[index][index]));
					}
				}
				accumulator -= dt;
			}
		}

		/*TASK 4*/
		else if (task4) {
			GLfloat newTime = (GLfloat)glfwGetTime();
			GLfloat frameTime = newTime - startTime;
			startTime = newTime;
			accumulator += frameTime;

			while (accumulator >= dt) {
				glm::vec3 Fwind = glm::vec3(0.0f);

				float particleYpos = particleBlowDryer.getPos()[1];
				float particleXZpos = glm::length(glm::vec2(particleBlowDryer.getPos()[0], particleBlowDryer.getPos()[2]));
				float newConeRadius = coneAngleTang * particleYpos;

				// Check if the particle is inside the cone.
				if ((particleYpos <= coneHeight) && (particleXZpos <= newConeRadius)) {
					float windStrongnessCoefficient = (1.0f / (particleYpos + 1.0f));
					float windSpeed = glm::length(windVel * (1.0f / (particleXZpos + 1.0f)));
					glm::vec3 windDirection = particleBlowDryer.getPos() / glm::length(particleBlowDryer.getPos());
					Fwind = windStrongnessCoefficient * (0.5f * airDensity * (windSpeed * windSpeed) * 0.1 * windDirection);
				}
				particleBlowDryer.setAcc((Fg + Fwind) / particleBlowDryer.getMass());
				particleBlowDryer.setVel(particleBlowDryer.getVel() + dt * particleBlowDryer.getAcc());
				particleBlowDryer.translate(dt * particleBlowDryer.getVel());
				// Check for collisions
				for (int index = 0; index <= 2; index++) {
					if (particleBlowDryer.getPos()[index] < (cube.getPos()[index] - 0.5f * cube.getScale()[index][index])) {
						particleBlowDryer.setVel(index, particleBlowDryer.getVel()[index] * -1.0f);
						particleBlowDryer.setVel(particleBlowDryer.getVel() * 0.95f);
						particleBlowDryer.setPos(index, (cube.getPos()[index] - 0.5f * cube.getScale()[index][index]));
					}
					else if (particleBlowDryer.getPos()[index] > (cube.getPos()[index] + 0.5f * cube.getScale()[index][index])) {
						particleBlowDryer.setVel(index, particleBlowDryer.getVel()[index] * -1.0f);
						particleBlowDryer.setVel(particleBlowDryer.getVel() * 0.95f);
						particleBlowDryer.setPos(index, (cube.getPos()[index] + 0.5f * cube.getScale()[index][index]));
					}
				}
				accumulator -= dt;
			}
		}
		/********************************************************/

		/*
		**	RENDER 
		*/		
		// clear buffer
		app.clear();
		// draw groud plane
		app.draw(plane);
		
		if (task1) {
			/*Task 1*/
			app.draw(boxParticle.getMesh());
		}
		else if (task2) {
			/*Task 2*/
			app.draw(boxParticle2.getMesh());
		}
		else if (task3) {
			/*Task 3*/
			app.draw(sieBoxParticle.getMesh());
			app.draw(forwardBoxParticle.getMesh());
			app.draw(immobileBoxParticle.getMesh());
		}
		else if (task4) {
			/*Task 4*/
			app.draw(particleBlowDryer.getMesh());
			app.draw(cone);
		}

		// Draw the cube objects
		app.draw(cube);

		app.display();
	}

	app.terminate();

	return EXIT_SUCCESS;
}

