# Projectile motion and Newtonian physics - Physics-Based Animations

When the application is started the active camera is free camera. With it the user can go around the scene and explore the different graphics effect. 

In order to move the camera you have to use the following controls: 

* straight ahead - W
* backwards - S
* right - D
* left - A
* rotate the camera - mouse

The following keys show the following tasks:

* Key 1 - Particle bouncing within cube with delta time equal to the time needed for the game loop.

![](https://bitbucket.org/dimitarshr/projectile-motion-and-newtonian-physics/raw/50b0e24f64f6a3805cd4b45b054dab9fc6a87ea2/demos/task1.gif)

---
* Key 2 - Particle bouncing within cube with constant delta time equal to 0.01s per frame.

![](https://bitbucket.org/dimitarshr/projectile-motion-and-newtonian-physics/raw/50b0e24f64f6a3805cd4b45b054dab9fc6a87ea2/demos/task2.gif)

---
* Key 3 - Visual comparison between *Forward* and *Semi-implicit Euler* integration methods.

![](https://bitbucket.org/dimitarshr/projectile-motion-and-newtonian-physics/raw/50b0e24f64f6a3805cd4b45b054dab9fc6a87ea2/demos/task3.gif)

---
* Key 4 - Particle bouncing within cube under the effect of blow dryer.

![](https://bitbucket.org/dimitarshr/projectile-motion-and-newtonian-physics/raw/50b0e24f64f6a3805cd4b45b054dab9fc6a87ea2/demos/task4.gif)

---

In order to compile the project under *linux* use:
```g++ -std=c++11 main.cpp Application.cpp Body.cpp Particle.cpp Mesh.cpp OBJLoader.cpp -lGLEW -lglfw -lGL -o output```